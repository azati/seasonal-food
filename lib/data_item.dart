import 'package:flutter/cupertino.dart';

class DataItem {
  List<Image> images;
  String title;
  String body;
  String country;
  String month;

  double energy;

  DataItem(this.images, this.title, this.body, this.month, this.country, this.energy);
}
