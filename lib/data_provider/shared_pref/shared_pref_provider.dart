import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefProvider {
  Future<bool> isFirstTimeLaunch() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool firstTime = prefs.getBool('first_time');
    if (firstTime != null && !firstTime) {
      // Not first time
      return false;
    } else {
      // First time
      return true;
    }
  }

  Future<void> setLaunchStatus(bool status) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('first_time', status);
  }

  Future<int> getSavedCurrentCountryId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int id = prefs.getInt('current_country');
    if (id != null)
      return id;
    else
      return -1;
  }

  Future<void> saveCurrentCountryId(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('current_country', id);
  }
}
