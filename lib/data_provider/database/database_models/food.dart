class Food {
  int id;
  String name;
  String locale;
  String code;

  double energy;

//  Double fat;
//  Double satFat;
//  Double transFat;
//  Double cholesterol;
//  Double sodium;
//  Double totalCarbs;
//  Double fiber;
//  Double sugar;
//  Double protein;
//  Double vitaminA;
//  Double vitaminC;
//  Double calcium;
//  Double iron;
//  Double polyUnsatFat;
//  Double monoUnsatFat;
//  Double omega3Fat;
//  Double netCarbs;
//  Double sugarAlcohol;
//  Double alcohol;
//  Double potassium;
//  Double caffeine;

  Food({this.id, this.code, this.locale, this.name, this.energy
//    this.fat,
//    this.satFat,
//    this.transFat,
//    this.cholesterol,
//    this.sodium,
//    this.totalCarbs,
//    this.fiber,
//    this.sugar,
//    this.protein,
//    this.vitaminA,
//    this.vitaminC,
//    this.calcium,
//    this.iron,
//    this.polyUnsatFat,
//    this.monoUnsatFat,
//    this.omega3Fat,
//    this.netCarbs,
//    this.sugarAlcohol,
//    this.alcohol,
//    this.potassium,
//    this.caffeine
      });

  factory Food.fromMap(Map<String, dynamic> json) => new Food(
        id: json["food_id"],
        code: json["code"],
        name: json["name"],
        locale: json["locale"],

        energy: json["energy"],
//    fat: json["fat"],
//    satFat: json["satFat"],
//    transFat: json["transFat"],
//    cholesterol: json["cholesterol"],
//    sodium: json["sodium"],
//    totalCarbs: json["totalCarbs"],
//    fiber: json["fiber"],
//    sugar: json["sugar"],
//    protein: json["protein"],
//    vitaminA: json["vitaminA"],
//    vitaminC: json["vitaminC"],
//    calcium: json["calcium"],
//    iron: json["iron"],
//    polyUnsatFat: json["polyUnsatFat"],
//    monoUnsatFat: json["monoUnsatFat"],
//    omega3Fat: json["omega3Fat"],
//    netCarbs: json["netCarbs"],
//    sugarAlcohol: json["sugarAlcohol"],
//    alcohol: json["alcohol"],
//    potassium: json["potassium"],
//    caffeine: json["caffeine"],
      );
}
