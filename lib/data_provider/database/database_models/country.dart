class Country {
  int id;
  String name;
  String code;

  Country({this.id, this.code, this.name});

  factory Country.fromMap(Map<String, dynamic> json) => new Country(
      id: json["country_id"], code: json["code"], name: json["name"]);
}
