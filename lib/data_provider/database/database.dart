import 'dart:io';
import 'dart:typed_data';

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:seasonalfood/data_provider/database/database_models/country.dart';
import 'package:seasonalfood/data_provider/database/database_models/food.dart';
import 'package:seasonalfood/data_provider/database/database_models/month.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  Database _database;
  String _locale = "en";

  final _dbMonths = [
    "january",
    "february",
    "march",
    "april",
    "may",
    "june",
    "july",
    "august",
    "september",
    "october",
    "november",
    "december"
  ];

  static final DatabaseProvider _singleton = DatabaseProvider._internal();

  factory DatabaseProvider() {
    return _singleton;
  }

  DatabaseProvider._internal();

  Future<void> init() async {
    String deviceLocale = (await Devicelocale.currentLocale).substring(0, 2);
    final db = await database;
    var res = await db.rawQuery(
        "SELECT * FROM FoodTranslation WHERE locale = ?", [deviceLocale]);
    if (res.length > 0) _locale = deviceLocale;
  }

  Future<Database> initDB() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "sf.sqlite");

    var exists = await databaseExists(path);

    if (!exists) {
      print("Creating new copy from asset");
      try {
        await Directory(dirname(path)).create(recursive: true);
      } catch (_) {}

      ByteData data = await rootBundle.load(join("assets", "sf.sqlite"));
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      await File(path).writeAsBytes(bytes, flush: true);
    } else {
      print("Opening existing database");
    }
    var db = await openDatabase(path, readOnly: true);
    return db;
  }

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  Future<List<Food>> getAllTranslatedFood() async {
    final db = await database;
    var res = await db.rawQuery(
        "SELECT food_id, name, locale, code, energy FROM FoodTranslation INNER JOIN Food ON food_id = Food.id WHERE locale = ?",
        [_locale]);
    List<Food> list =
        res.isNotEmpty ? res.map((c) => Food.fromMap(c)).toList() : [];
    return list;
  }

  Future<List<Country>> getTranslatedCountries() async {
    final db = await database;
    var res = await db.rawQuery(
        "SELECT country_id, name, code FROM CountryTranslation INNER JOIN Country ON country_id = Country.id WHERE locale = ?",
        [_locale]);
    List<Country> list =
        res.isNotEmpty ? res.map((c) => Country.fromMap(c)).toList() : [];
    return list;
  }

  Future<List<Food>> getFood(Month month, int countryId) async {
    final db = await database;
    var monthIndex = month.index;
    var res = await db.rawQuery(
        "SELECT FoodTranslation.food_id AS food_id, name, locale, code, energy FROM FoodTranslation INNER JOIN Food ON FoodTranslation.food_id = Food.id INNER JOIN Calendar ON FoodTranslation.food_id = Calendar.food_id WHERE locale = ? AND country_id = $countryId AND ${_dbMonths[monthIndex]} > 0",
        [_locale]);
    List<Food> list =
        res.isNotEmpty ? res.map((c) => Food.fromMap(c)).toList() : [];
    return list;
  }

  List<Month> getMonths() {
    if (_locale == "ru") {
      return [
        Month(index: 0, name: "Январь"),
        Month(index: 1, name: "Февраль"),
        Month(index: 2, name: "Март"),
        Month(index: 3, name: "Апрель"),
        Month(index: 4, name: "Май"),
        Month(index: 5, name: "Июнь"),
        Month(index: 6, name: "Июль"),
        Month(index: 7, name: "Август"),
        Month(index: 8, name: "Сентябрь"),
        Month(index: 9, name: "Октябрь"),
        Month(index: 10, name: "Ноябрь"),
        Month(index: 11, name: "Декабрь")
      ];
    } else {
      return [
        Month(index: 0, name: "January"),
        Month(index: 1, name: "February"),
        Month(index: 2, name: "March"),
        Month(index: 3, name: "April"),
        Month(index: 4, name: "May"),
        Month(index: 5, name: "June"),
        Month(index: 6, name: "July"),
        Month(index: 7, name: "August"),
        Month(index: 8, name: "September"),
        Month(index: 9, name: "October"),
        Month(index: 10, name: "November"),
        Month(index: 11, name: "December")
      ];
    }
  }
}
