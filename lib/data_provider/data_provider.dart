import 'package:seasonalfood/data_provider/database/database.dart';
import 'package:seasonalfood/data_provider/database/database_models/country.dart';
import 'package:seasonalfood/data_provider/database/database_models/food.dart';
import 'package:seasonalfood/data_provider/database/database_models/month.dart';
import 'package:seasonalfood/data_provider/shared_pref/shared_pref_provider.dart';

class DataProvider {
  List<Country> _cachedCountries;
  DatabaseProvider _database;
  SharedPrefProvider _sharedPref;

  static final DataProvider _singleton = DataProvider._internal();

  factory DataProvider() {
    return _singleton;
  }

  DataProvider._internal();

  Future<void> init() async {
    _database = DatabaseProvider();
    _sharedPref = SharedPrefProvider();
    await _database.init();
    await getCountries();
  }

  Future<List<Country>> getCountries() async {
    if (_cachedCountries == null) {
      _cachedCountries = await _database.getTranslatedCountries();
    }
    return _cachedCountries;
  }

  Future<List<Food>> getFood(Month month, int countryId) async {
    return _database.getFood(month, countryId);
  }

  List<Month> getMonths() {
    return _database.getMonths();
  }

  Future<bool> isFirstTimeLaunch() async {
    return _sharedPref.isFirstTimeLaunch();
  }

  Future<void> setLaunchStatus(bool status) async {
    _sharedPref.setLaunchStatus(status);
  }

  Future<void> saveCurrentCountryId(int id) async {
    _sharedPref.saveCurrentCountryId(id);
  }

  Month getInitialMonth() {
    return getMonths()[DateTime.now().month - 1]; //текущий месяц
  }

  Future<Country> getSavedCountry() async {
    var countryInitialId = await _sharedPref.getSavedCurrentCountryId();
    var countries = await getCountries();
    if (countryInitialId != -1) {
      //если страна сохранялась
      countries.firstWhere((c) {
        return c.id == countryInitialId;
      });
      return countries.firstWhere((country) {
        return country.id == countryInitialId;
      });
    } else {
      //иначе пытаемся поставить великобританию как дефолт
      return countries.firstWhere((c) {
        return c.code == "gb";
      });
    }
  }
}
