import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:seasonalfood/admob/admob_provider.dart';
import 'package:seasonalfood/data_provider/data_provider.dart';
import 'package:seasonalfood/home_page.dart';
import 'package:seasonalfood/item_click_listener.dart';
import 'package:seasonalfood/localization/app_localizations_delegate.dart';

void main() => runApp(MyApp(Key("app"), dataProvider: DataProvider()));

class MyApp extends StatelessWidget {
  final DataProvider dataProvider;

  MyApp(Key key, {@required this.dataProvider}) : super(key: key);

  final analytics = FirebaseAnalytics();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', ''),
        const Locale('ru', ''),
      ],
      title: 'Flutter Demo',
      home: Home(
        key: Key("home"),
        dataProvider: dataProvider,
      ),
      navigatorObservers: [FirebaseAnalyticsObserver(analytics: analytics)],
    );
  }
}

class Home extends StatefulWidget {
  final DataProvider dataProvider;

  Home({Key key, @required this.dataProvider}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<Home> implements ItemClickListener {
  bool isDataProviderInit = false;
  Admob _admob;
  int openedItemsCount = 0;

  @override
  void initState() {
    super.initState();
    _admob = Admob();
    _admob.init();
    _initDataProvider();
  }

  @override
  Widget build(BuildContext context) {
    if (isDataProviderInit) {
      return HomePage(
          key: Key("homePage"),
          dataProvider: widget.dataProvider,
          itemClickListener: this);
    } else {
      return Scaffold(
        body: Center(
          child: CircularProgressIndicator(
            key: Key("homeProgressIndicator"),
          ),
        ),
      );
    }
  }

  Future<void> _initDataProvider() async {
    DataProvider dataProvider = widget.dataProvider;
    dataProvider.init().then((_) => setState(() {
          isDataProviderInit = true;
        }));
  }

  @override
  void onClick() {
    openedItemsCount++;
    if (openedItemsCount == 3) {
      _admob.showInterstitial();
    }
  }
}
