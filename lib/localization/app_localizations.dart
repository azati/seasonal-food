import 'dart:ui';

import 'package:flutter/widgets.dart';

class AppLocalizations {
  AppLocalizations(this._locale);

  final Locale _locale;

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'about': 'About',
      'month': 'Month',
      'country': 'Country',
      'select_country': 'Select country',
      'select_month': 'Select month',
      'loading': 'Loading',
      'energy': 'Energy'
    },
    'ru': {
      'about': 'О приложении',
      'month': 'Месяц',
      'country': 'Страна',
      'select_country': 'Выберите страну',
      'select_month': 'Выберите месяц',
      'loading': 'Загрузка',
      'energy': 'Энергетическая ценность'
    }
  };

  String get month {
    return _localizedValues[_locale.languageCode]['month'];
  }

  String get country {
    return _localizedValues[_locale.languageCode]['country'];
  }

  String get selectCountry {
    return _localizedValues[_locale.languageCode]['select_country'];
  }

  String get selectMonth {
    return _localizedValues[_locale.languageCode]['select_month'];
  }

  String get about {
    return _localizedValues[_locale.languageCode]['about'];
  }

  String get loading {
    return _localizedValues[_locale.languageCode]['loading'];
  }

  String get energy {
    return _localizedValues[_locale.languageCode]['energy'];
  }
}
