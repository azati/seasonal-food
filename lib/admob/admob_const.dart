import 'dart:io';

class AdmobConst {
  static const _ANDROID_APP_ID = "ca-app-pub-5394541178354202~2488634916";
  static const _IOS_APP_ID = "ca-app-pub-5394541178354202~7749534608";
  static const _ANDROID_INTERSTITIAL_KEY = "ca-app-pub-5394541178354202/2141984709";
  static const _IOS_INTERSTITIAL_KEY = "ca-app-pub-5394541178354202/7913186139";
  static const _ANDROID_BANNER_KEY = "";
  static const _IOS_BANNER_KEY = "";

  static String getAppId() {
    if (Platform.isAndroid) {
      return _ANDROID_APP_ID;
    } else {
      return _IOS_APP_ID;
    }
  }

  static String getBannerKey() {
    if (Platform.isAndroid) {
      return _ANDROID_BANNER_KEY;
    } else {
      return _IOS_BANNER_KEY;
    }
  }

  static String getInterstitialKey() {
    if (Platform.isAndroid) {
      return _ANDROID_INTERSTITIAL_KEY;
    } else {
      return _IOS_INTERSTITIAL_KEY;
    }
  }
}
