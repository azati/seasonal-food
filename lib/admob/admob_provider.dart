import 'package:firebase_admob/firebase_admob.dart';
import 'package:seasonalfood/admob/admob_const.dart';

class Admob {
  InterstitialAd _interstitialAd;

  init() async {
    await FirebaseAdMob.instance.initialize(appId: AdmobConst.getAppId());
//    _initBanner();
    _initInterstitial();
  }

  void _initBanner() {
    var bannerAd = BannerAd(
      adUnitId: AdmobConst.getBannerKey(),
      size: AdSize.smartBanner,
      targetingInfo: MobileAdTargetingInfo(),
      listener: (MobileAdEvent event) {
        print("BannerAd event is $event");
      },
    );
    bannerAd
      ..load()
      ..show(
        anchorType: AnchorType.bottom,
      );
  }

  void _initInterstitial() {
    _interstitialAd = InterstitialAd(
      adUnitId: AdmobConst.getInterstitialKey(),
      targetingInfo: MobileAdTargetingInfo(),
      listener: (MobileAdEvent event) {
        print("InterstitialAd event is $event");
      },
    );

    _interstitialAd.load();
  }

  void showInterstitial() {
    _interstitialAd.show();
  }
}
