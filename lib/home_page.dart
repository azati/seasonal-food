import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:seasonalfood/data_provider/data_provider.dart';
import 'package:seasonalfood/data_provider/database/database_models/country.dart';
import 'package:seasonalfood/data_provider/database/database_models/food.dart';
import 'package:seasonalfood/data_provider/database/database_models/month.dart';
import 'package:seasonalfood/item_click_listener.dart';
import 'package:seasonalfood/localization/app_localizations.dart';

import 'data_item.dart';
import 'detail_item_page.dart';

class HomePage extends StatefulWidget {
  final DataProvider dataProvider;
  final ItemClickListener itemClickListener;

  HomePage({Key key, @required this.dataProvider, this.itemClickListener})
      : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  List<DataItem> items;
  Country currentCountry;
  Month currentMonth;

  @override
  void initState() {
    super.initState();

    currentMonth = widget.dataProvider.getInitialMonth();
    widget.dataProvider.getSavedCountry().then(_onInitialCountryLoaded);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key("homePageScaffold"),
      appBar: AppBar(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Seasonal food"),
            Text(
                currentMonth != null
                    ? currentMonth.name
                    : AppLocalizations.of(context).loading,
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                key: Key("appbarMonthName"))
          ],
        ),
        actions: currentCountry != null
            ? <Widget>[
                Container(
                  margin: EdgeInsets.all(6),
                  child: IconButton(
                    key: Key("appbarCountryIcon"),
                    onPressed: () {
                      _onClickSelectCountry();
                    },
                    icon: Image(
                      image: _getCurrentCountryFlagIcon(),
                    ),
                  ),
                )
              ]
            : null,
      ),
      drawer: Drawer(
        key: Key("drawer"),
        child: Container(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                    child: new Image(
                  image: new AssetImage('assets/potato.jpg'),
                )),
                ListTile(
                  key: Key("selectMonthButton"),
                  leading: Icon(Icons.calendar_today),
                  title: Text(AppLocalizations.of(context).month),
                  onTap: () {
                    Navigator.of(context).pop();
                    _onClickSelectMonth();
                  },
                ),
                ListTile(
                  key: Key("selectCountryButton"),
                  leading: Icon(Icons.language),
                  title: Text(AppLocalizations.of(context).country),
                  onTap: () {
                    Navigator.of(context).pop();
                    _onClickSelectCountry();
                  },
                ),
                Divider(height: 1),
                ListTile(
                  leading: Icon(Icons.info),
                  title: Text(AppLocalizations.of(context).about),
                  onTap: () {},
                )
              ],
            ),
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(bottom: 50),
        child: Center(
          child: currentMonth != null && currentCountry != null
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: _buildItemsWidget(),
                    ),
                  ],
                )
              : CircularProgressIndicator(),
        ),
      ),
    );
  }

  void _onInitialCountryLoaded(Country initialCounty) {
    setState(() {
      currentCountry = initialCounty;
    });
    widget.dataProvider
        .isFirstTimeLaunch()
        .then((isFirstLaunch) => setState(() {
              if (isFirstLaunch) {
                widget.dataProvider.setLaunchStatus(false);
                _onClickSelectCountry();
              }
            }));
    _initFoodItems();
  }

  void _initFoodItems() async {
    final food =
        await widget.dataProvider.getFood(currentMonth, currentCountry.id);
    setState(() {
      items = _foodToDataItem(food);
    });
  }

  ImageProvider _getCurrentCountryFlagIcon() {
    return AssetImage('assets/flags/flag_128_${currentCountry.code}.png');
  }

  Widget _buildItemsWidget() {
    return items != null
        ? ListView.separated(
            itemCount: items.length,
            itemBuilder: (BuildContext context, int index) {
              return _buildItem(items[index], index);
            },
            separatorBuilder: (context, index) {
              return Divider(height: 1);
            })
        : Center(child: CircularProgressIndicator());
  }

  Widget _buildItem(DataItem dataItem, int index) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: InkWell(
        key: Key("foodItem$index"),
        child: Container(
            margin: EdgeInsets.all(8),
            child: Column(
              children: <Widget>[
                Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: 75,
                          height: 75,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: Hero(
                                tag: "image" + dataItem.title,
                                child: dataItem.images.first),
                          )),
                      Container(
                        child: Text(dataItem.title,
                            key: Key("foodItemText$index"),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 36)),
                        margin: EdgeInsets.only(left: 8, right: 8, bottom: 4),
                      )
                    ])
              ],
            )),
        onTap: () {
          widget.itemClickListener.onClick();
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => DetailItemPage(dataItem)),
          );
        },
      ),
    );
  }

  void _onClickSelectMonth() {
    var months = widget.dataProvider.getMonths();
    if (Platform.isAndroid) {
      _onClickSelectMonthAndroid(months);
    } else {
      _onClickSelectMonthIos(months);
    }
  }

  void _onClickSelectMonthAndroid(List<Month> months) async {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              key: Key("selectMonthDialog"),
              title: Text(AppLocalizations.of(context).selectMonth),
              content: Container(
                width: double.maxFinite,
                child: ListView.separated(
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return ListTile(
                        key: Key("monthItem$index"),
                        title: Text(months[index].name),
                        onTap: () async {
                          _onMonthSelected(months[index]);
                        },
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider(height: 1);
                    },
                    itemCount: months.length),
              ),
            ));
  }

  void _onClickSelectMonthIos(List<Month> months) async {
    int _selectedMonthIndex = months[currentMonth.index].index;
    showCupertinoModalPopup(
        context: context,
        builder: (context) => Container(
              key: Key("selectMonthDialog"),
              height: 200.0,
              color: Colors.white,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CupertinoButton(
                    child: Text("Cancel"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  Expanded(
                    child: CupertinoPicker(
                        scrollController: new FixedExtentScrollController(
                          initialItem: months[currentMonth.index].index,
                        ),
                        itemExtent: 32.0,
                        backgroundColor: Colors.white,
                        onSelectedItemChanged: (int index) {
                          _selectedMonthIndex = index;
                        },
                        children: months.map((e) => Text(e.name)).toList()),
                  ),
                  CupertinoButton(
                    key: Key("confirmMonthButton"),
                    child: Text("Ok"),
                    onPressed: () {
                      _onMonthSelected(months[_selectedMonthIndex]);
                    },
                  ),
                ],
              ),
            ));
  }

  void _onMonthSelected(Month month) async {
    currentMonth = month;

    List<Food> foodFiltered =
        await widget.dataProvider.getFood(currentMonth, currentCountry.id);
    items = _foodToDataItem(foodFiltered);

    Navigator.of(context).pop();

    setState(() {});
  }

  void _onClickSelectCountry() async {
    var countries = await widget.dataProvider.getCountries();
    if (Platform.isAndroid) {
      _onSelectCountryAndroid(countries);
    } else {
      _onSelectCountryIos(countries);
    }
  }

  void _onSelectCountryAndroid(List<Country> countries) async {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              key: Key("selectCountryDialog"),
              title: Text(AppLocalizations.of(context).selectCountry),
              content: Container(
                width: double.maxFinite,
                child: ListView.separated(
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return ListTile(
                        key: Key("countryItem$index"),
                        title: Text(countries[index].name),
                        onTap: () async {
                          _onCountrySelected(countries[index]);
                        },
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider(height: 1);
                    },
                    itemCount: countries.length),
              ),
            ));
  }

  void _onSelectCountryIos(List<Country> countries) async {
    int _selectedCountryIndex = countries.indexOf(currentCountry);

    showCupertinoModalPopup(
        context: context,
        builder: (context) => Container(
              key: Key("selectCountryDialog"),
              height: 200.0,
              color: Colors.white,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CupertinoButton(
                    child: Text("Cancel"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  Expanded(
                    child: CupertinoPicker(
                        scrollController: new FixedExtentScrollController(
                          initialItem: countries.indexOf(currentCountry),
                        ),
                        itemExtent: 32.0,
                        backgroundColor: Colors.white,
                        onSelectedItemChanged: (int index) {
                          _selectedCountryIndex = index;
                        },
                        children: countries
                            .map((country) => Text(country.name))
                            .toList()),
                  ),
                  CupertinoButton(
                    child: Text("Ok"),
                    key: Key("confirmCountryButton"),
                    onPressed: () {
                      _onCountrySelected(countries[_selectedCountryIndex]);
                    },
                  ),
                ],
              ),
            ));
  }

  void _onCountrySelected(Country country) async {
    currentCountry = country;

    List<Food> foodFiltered =
        await widget.dataProvider.getFood(currentMonth, country.id);
    items = _foodToDataItem(foodFiltered);

    widget.dataProvider.saveCurrentCountryId(country.id);
    Navigator.of(context).pop();

    setState(() {});
  }

  List<DataItem> _foodToDataItem(List<Food> food) {
    return food
        .map((foodItem) => DataItem([
              Image(
                  image: AssetImage('assets/food/food_${foodItem.code}.jpg'),
                  fit: BoxFit.scaleDown)
            ], foodItem.name, "МНОГО ТЕКСТА", currentMonth.name,
                foodItem.locale, foodItem.energy))
        .toList();
  }
}
