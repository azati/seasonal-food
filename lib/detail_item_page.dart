import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:seasonalfood/data_item.dart';
import 'package:seasonalfood/localization/app_localizations.dart';

class DetailItemPage extends StatelessWidget {
  final DataItem item;

  DetailItemPage(this.item);

  final controller = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    var pageView = PageView.builder(
      controller: controller,
      itemCount: item.images.length,
      itemBuilder: (BuildContext context, int index) {
        if (index == 0) {
          return Hero(
            key: Key("productImage$index"),
            tag: "image" + item.title,
            child: item.images[index],
          );
        }
        return item.images[index];
      },
    );

    return Scaffold(
      appBar: AppBar(title: Text(item.title, key: Key("detailItemTitle"))),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              child: pageView,
            ),
//            Row(
//              children: <Widget>[
//                Container(
//                    margin: EdgeInsets.all(8),
//                    child: Text(AppLocalizations.of(context).country + ": ",
//                        style: TextStyle(fontWeight: FontWeight.bold))),
//                Container(
//                    margin: EdgeInsets.all(8),
//                    child: Text(item.country, key: Key("detailItemCountry"))),
//              ],
//            ),
//            Row(
//              children: <Widget>[
//                Container(
//                    margin: EdgeInsets.all(8),
//                    child: Text(AppLocalizations.of(context).month + ": ",
//                        style: TextStyle(fontWeight: FontWeight.bold))),
//                Container(
//                    margin: EdgeInsets.all(8),
//                    child: Text(item.month, key: Key("detailItemMonth"))),
//              ],
//            ),
            Row(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.all(8),
                    child: Text(AppLocalizations.of(context).energy + ": ", style: TextStyle(fontWeight: FontWeight.bold))
                ),
                Container(
                    margin: EdgeInsets.all(8),
                    child: Text((item.energy / 4.184).toStringAsFixed(0) + ' cal', key: Key("detailItemEnergy"))
                ),
              ],
            ),
//            Container(margin: EdgeInsets.all(8), child: Text(item.body, key: Key("detailItemBody")))
          ],
        ),
      ),
    );
  }
}
