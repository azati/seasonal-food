import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:seasonalfood/data_provider/data_provider.dart';
import 'package:seasonalfood/data_provider/database/database_models/country.dart';
import 'package:seasonalfood/data_provider/database/database_models/food.dart';
import 'package:seasonalfood/data_provider/database/database_models/month.dart';
import 'package:seasonalfood/home_page.dart';
import 'package:seasonalfood/localization/app_localizations_delegate.dart';
import 'package:seasonalfood/main.dart';

class DataProviderMock extends Mock implements DataProvider {}

void main() {
  testWidgets('IF first launch THEN show select country dialog',
      (WidgetTester tester) async {
    const homeKey = Key("home");
    var dataProvider = createDataProviderMock(isFirstLaunch: true);

    var home = createTestWidget(Home(key: homeKey, dataProvider: dataProvider));

    await tester.pumpWidget(home);
    await tester.pump();

    const homePageKey = Key("homePage");
    var homePage = find.byKey(homePageKey);
    expect(homePage, findsOneWidget);

    await tester.pump();

    const selectCountyDialogKey = Key("selectCountryDialog");
    var selectCountryDialog = find.byKey(selectCountyDialogKey);
    expect(selectCountryDialog, findsOneWidget);
  });

  testWidgets('IF not first launch THEN don\'t show select country dialog',
      (WidgetTester tester) async {
    const homeKey = Key("home");
    var dataProvider = createDataProviderMock(isFirstLaunch: false);

    var home = createTestWidget(Home(key: homeKey, dataProvider: dataProvider));

    await tester.pumpWidget(home);
    await tester.pump();

    const homePageKey = Key("homePage");
    var homePage = find.byKey(homePageKey);
    expect(homePage, findsOneWidget);

    await tester.pump();

    const selectCountyDialogKey = Key("selectCountryDialog");
    var selectCountryDialog = find.byKey(selectCountyDialogKey);
    expect(selectCountryDialog, findsNothing);
  });

  testWidgets('IF change month THEN month changed',
      (WidgetTester tester) async {
    var dataProvider = createDataProviderMock(isFirstLaunch: false);

    var home =
        createTestWidget(Home(key: Key("home"), dataProvider: dataProvider));

    await tester.pumpWidget(home);
    await tester.pump();

    var homePage = find.byKey(Key("homePage"));
    expect(homePage, findsOneWidget);

    await tester.pump();

    expect(find.text(_fakeMonths.elementAt(1).name), findsOneWidget);

    await tester.tap(find.byTooltip('Open navigation menu'));
    await tester.pumpAndSettle();

    expect(find.byIcon(Icons.calendar_today), findsOneWidget);

    await tester.tap(find.byIcon(Icons.calendar_today));
    await tester.pump();

    expect(find.byKey(Key("selectMonthDialog")), findsOneWidget);

    await tester.drag(
        find.text(_fakeMonths.elementAt(1).name).last, Offset(0, -50));
    await tester.pumpAndSettle();

    await tester.drag(
        find.text(_fakeMonths.elementAt(1).name).last, Offset(0, 50));
    await tester.pumpAndSettle();

    CupertinoButton confirmMonthButtonWidget =
        find.byKey(Key("confirmMonthButton")).evaluate().first.widget;
    confirmMonthButtonWidget.onPressed();

    await tester.pumpAndSettle();

    expect(find.byKey(Key("selectMonthDialog")), findsNothing);

    expect(find.text(_fakeMonths.elementAt(1).name), findsNothing);
    expect(find.text(_fakeMonths.elementAt(0).name), findsOneWidget);

    verify(dataProvider.getFood(
            _fakeMonths.elementAt(0), _fakeCountries.elementAt(1).id))
        .called(1);
  });

  testWidgets('IF change country THEN country changed',
      (WidgetTester tester) async {
    var dataProvider = createDataProviderMock(isFirstLaunch: false);

    var home =
        createTestWidget(Home(key: Key("home"), dataProvider: dataProvider));

    await tester.pumpWidget(home);
    await tester.pump();

    var homePage = find.byKey(Key("homePage"));
    HomePageState homePageState = tester.state(find.byKey(Key("homePage")));
    expect(homePageState.currentCountry, _fakeCountries.elementAt(1));
    expect(homePage, findsOneWidget);

    await tester.pump();

    expect(find.text(_fakeMonths.elementAt(1).name), findsOneWidget);

    await tester.tap(find.byTooltip('Open navigation menu'));
    await tester.pumpAndSettle();

    expect(find.byIcon(Icons.language), findsOneWidget);

    await tester.tap(find.byIcon(Icons.language));
    await tester.pump();

    expect(find.byKey(Key("selectCountryDialog")), findsOneWidget);

    await tester.drag(
        find.text(_fakeCountries.elementAt(1).name).last, Offset(0, -50));
    await tester.pumpAndSettle();

    await tester.drag(
        find.text(_fakeCountries.elementAt(1).name).last, Offset(0, 50));
    await tester.pumpAndSettle();

    CupertinoButton confirmMonthButtonWidget =
        find.widgetWithText(CupertinoButton, "Ok").evaluate().first.widget;
    confirmMonthButtonWidget.onPressed();

    await tester.pumpAndSettle();

    expect(find.byKey(Key("selectCountryDialog")), findsNothing);
    expect(homePageState.currentCountry, _fakeCountries.elementAt(0));

    verify(dataProvider.getFood(
            _fakeMonths.elementAt(1), _fakeCountries.elementAt(0).id))
        .called(1);
  });

  testWidgets('IF click on food THEN open food detail page',
      (WidgetTester tester) async {
    var dataProvider = createDataProviderMock(isFirstLaunch: false);

    var home =
        createTestWidget(Home(key: Key("home"), dataProvider: dataProvider));

    await tester.pumpWidget(home);
    await tester.pump();

    HomePageState homePageState = tester.state(find.byKey(Key("homePage")));

    final pageFoodItem = homePageState.items.first;

    await tester.pump();

    final foodItem0 = find.byType(InkWell).first;

    await tester.tap(foodItem0);
    await tester.pumpAndSettle();

    expect(find.text(pageFoodItem.title), findsOneWidget);
    expect(find.text(pageFoodItem.country), findsOneWidget);
    expect(find.text(pageFoodItem.month), findsOneWidget);
    expect(find.text(pageFoodItem.body), findsOneWidget);
  });
}

DataProvider createDataProviderMock({bool isFirstLaunch = false}) {
  var dp = DataProviderMock();

  when(dp.init()).thenAnswer((_) => Future.value());
  when(dp.getCountries()).thenAnswer((_) => Future.value(_fakeCountries));
  when(dp.getMonths()).thenReturn(_fakeMonths);
  when(dp.getSavedCountry())
      .thenAnswer((_) => Future.value(_fakeCountries.elementAt(1)));
  when(dp.getFood(any, any)).thenAnswer((_) => Future.value([]));
  when(dp.getInitialMonth()).thenReturn(_fakeMonths.elementAt(1));

  when(dp.isFirstTimeLaunch()).thenAnswer((_) => Future.value(isFirstLaunch));

  when(dp.getFood(_fakeMonths.elementAt(0), _fakeCountries.elementAt(0).id))
      .thenAnswer((_) => Future.value(_fakeFoodsForMonth0Country0));
  when(dp.getFood(_fakeMonths.elementAt(0), _fakeCountries.elementAt(1).id))
      .thenAnswer((_) => Future.value(_fakeFoodsForMonth0Country1));
  when(dp.getFood(_fakeMonths.elementAt(1), _fakeCountries.elementAt(1).id))
      .thenAnswer((_) => Future.value(_fakeFoodsForMonth1Country1));

  return dp;
}

Widget createTestWidget(Widget testWidget) {
  return MediaQuery(
      data: MediaQueryData(),
      child: MaterialApp(
        home: testWidget,
        localizationsDelegates: [
          const AppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
      ));
}

List<Country> _fakeCountries = [
  Country(id: 0, name: "Russia", code: "ru"),
  Country(id: 1, name: "United States of America", code: "us"),
  Country(id: 2, name: "United Kingdom", code: "gb")
];

List<Month> _fakeMonths = [
  Month(index: 0, name: "april"),
  Month(index: 1, name: "october"),
  Month(index: 2, name: "december"),
];

List<Food> _fakeFoodsForMonth0Country1 = [
  Food(id: 0, name: "Бананы", locale: "ru", code: "bananas"),
  Food(id: 1, name: "Яблоки", locale: "ru", code: "apples"),
  Food(id: 2, name: "Авокадо", locale: "ru", code: "avocado")
];

List<Food> _fakeFoodsForMonth1Country1 = [
  Food(id: 1, name: "Яблоки", locale: "ru", code: "apples"),
  Food(id: 2, name: "Авокадо", locale: "ru", code: "avocado")
];

List<Food> _fakeFoodsForMonth0Country0 = [
  Food(id: 0, name: "Бананы", locale: "ru", code: "bananas")
];
