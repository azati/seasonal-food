# seasonalfood

# Сборка DEV

## Для обновления зависимостей (локальных и удаленных)

1. Перейти в папку проекта в терминале и выполнить `flutter pub get`

## Для добавления новых файлов в ассеты

1. Открыть файл pubspec.yaml
2. Найти поле "assets:" и добавить в него нужные файлы, указав путь к ним. для этого можно использовать утилиту `bin/dump_dev_assets.sh` 
3. Перейти в папку проекта в терминале и выполнить `flutter pub get`

## Для переводов

1. В файл app_localizations_delegate.dart добавить новую локаль в список
2. В файле app_locatiozations.dart добавить новую локаль и все переводы для нее в общую мапу.
3. Если добавляются новые поля, то создаить их и для остальных локалей. Создать геттеры для них.
4. В коде получать переведенный текст следующим образом `AppLocalizations.of(context).{геттер для этого поля}}),`

## Для изменения иконки приложения

1. Открыть файл pubspec.yaml
2. Найти поле `flutter_icons:` и указать путь к иконкам
3. Перейти в папку проекта в терминале и выполнить `flutter pub get`
4. Выполнить `flutter packages pub run flutter_launcher_icons:main`

# Сборка PROD

Privacy policy: https://app-privacy-policy-generator.firebaseapp.com/

## Android

- поправить номер версии в pubspec.yaml `version:`
- создать файл android/key.properties

```
storePassword=demoflutter
keyPassword=demoflutter
keyAlias=keyalg
storeFile=/Users/user/key.store
```

- Далее по инструкции https://flutter.dev/docs/deployment/android
- Сборка с помощью `flutter build appbundle` или `flutter build apk`

### настройка ADMOB

1. Добавить файл google-services.json в папку .android/app
2. В [AndroidManifest.xml](android/app/src/main/AndroidManifest.xml) добавить id приложения как на примере https://pub.dev/packages/firebase_admob#-readme-tab-
3. Добавить этот же id в коде в файл admob_const.dart. Сюда же добавить ключи от рекламных блоков.

## IOS

- Может понадобиться `rm -rf ios/Flutter/App.framework`
- Сборка с помощью `flutter build ios`

### настройка ADMOB

1. Добавить в файл [Info.plist](ios/Runner/Info.plist) строку GADApplicationIdentifier с типом String и указать admob id приложения
2. Добавить этот же id в коде в файл admob_const.dart. Сюда же добавить ключи от рекламных блоков.
3. Добавить файл конфигурации в проект. Для этого в xcode жмем пкп на папке Runner/Runner, Add Files to Runner и выбираем файл конфигруации для ios

# Troubleshooting 

## Если проект не собирается или не запускается

1. Удалить папки ios и android из корня проекта
2. Перейти в папку проекта в терминале и выполнить `flutter create .` или `flutter create --org com.whyte624 seasonalfood`
3. Для ios может потребоваться установить поды командой `pod install`. Команда выполяется в папке ios

## Для решения прочих проблем может помочь

1. Перейти в папку проекта в терминале и выполнить `flutter clean`
2. Для починки зависимостей `flutter pub cache repair `

## Для запуска всех тестов сразу

1. Перейти в папку проекта в терминале и выполнить `flutter test`

## Для запуска интеграционных тестов

Должен быть запущен только один эмулятор. Ios или Android

Для запуска android тестов: `flutter driver --target=test_driver/android/app.dart`
Для запуска ios тестов: `flutter driver --target=test_driver/ios/app.dart`