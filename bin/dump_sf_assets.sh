# usage
# bin/dump_sf_assets.sh >> pubspec.yaml

sqlite3 assets/sf.sqlite <<EOS
  select '    - assets/flags/flag_128_' || code || '.png' from Country
  union
  select '    - assets/food/food_' || code || '.jpg' from Food
  union
  select '    - assets/food/food_' || code || '_64.jpg' from Food
EOS