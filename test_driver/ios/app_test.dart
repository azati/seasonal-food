import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  Future<bool> isPresent(SerializableFinder finder, FlutterDriver driver,
      {Duration timeout = const Duration(seconds: 1)}) async {
    try {
      await driver.waitFor(finder, timeout: timeout);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<List<String>> getFoodItemsTexts(FlutterDriver driver) async {
    List<String> texts = [];
    int index = 0;
    while (await isPresent(find.byValueKey("foodItem$index"), driver,
        timeout: Duration(milliseconds: 100))) {
      texts.add(await driver.getText(find.byValueKey("foodItemText$index")));
      index++;
    }
    return texts;
  }

  bool isEquals(List<String> list1, List<String> list2) {
    if (list1.length != list2.length) return false;
    bool isEqual = true;
    for (int i = 0; i < list1.length; i++) {
      if (list1[i] != list2[i]) {
        isEqual = false;
        break;
      }
    }
    return isEqual;
  }

  group('Seasonal food App', () {
    final selectCountryDialog = find.byValueKey("selectCountryDialog");
    final selectMonthDialog = find.byValueKey("selectMonthDialog");
    final firstCountryItem = find.byValueKey("countryItem0");
    final anyProductCard = find.byType("Card");
    final firstProductCard = find.byValueKey("foodItem0");
    final firstProductCardText = find.byValueKey("foodItemText0");

    FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Show any product items', () async {
      await driver.waitFor(selectCountryDialog);

      await driver.scroll(
          selectCountryDialog, 0, 100, Duration(milliseconds: 500));

      await driver.tap(find.byValueKey("confirmCountryButton"));

      await driver.waitFor(firstProductCard);

      await Future.delayed(Duration(seconds: 5));
    });

    test('Select country', () async {
      var previousTexts = await getFoodItemsTexts(driver);

      await driver.tap(find.byTooltip("Open navigation menu"));

      await Future.delayed(Duration(seconds: 1));

      await driver.tap(find.byValueKey("selectCountryButton"));

      await driver.scroll(
          selectCountryDialog, 0, -500, Duration(milliseconds: 500));

      await driver.tap(find.byValueKey("confirmCountryButton"));

      var newTexts = await getFoodItemsTexts(driver);

      var isTextsEquals = isEquals(previousTexts, newTexts);

      expect(isTextsEquals, isFalse);

      await Future.delayed(Duration(seconds: 5));
    });

    test('Select month', () async {
      var previousTexts = await getFoodItemsTexts(driver);

      await driver.tap(find.byTooltip("Open navigation menu"));

      await Future.delayed(Duration(seconds: 1));

      await driver.tap(find.byValueKey("selectMonthButton"));

      await driver.scroll(
          selectMonthDialog, 0, 300, Duration(milliseconds: 500));

      await driver.tap(find.byValueKey("confirmMonthButton"));

      var newTexts = await getFoodItemsTexts(driver);

      var isTextsEquals = isEquals(previousTexts, newTexts);

      expect(isTextsEquals, isFalse);

      await Future.delayed(Duration(seconds: 5));
    });

    test("Open any product", () async {
      await driver.tap(firstProductCard);

      expect(
          await driver.getText(find.byValueKey("detailItemTitle")), isNotEmpty);
      //expect(await driver.getText(find.byValueKey("detailItemBody")), isNotEmpty);
      await driver.waitFor(find.byValueKey("productImage0"));

      await Future.delayed(Duration(seconds: 5));
    });
  });
}
